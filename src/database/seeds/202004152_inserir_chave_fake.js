
exports.seed = function(knex, Promise) {
  return knex('chave').del()
    .then(function () {
      return knex('chave').insert([
        {titulo: 'Titulo 1', id_item: 1},
        {titulo: 'Titulo 2', id_item: 2},
        {titulo: 'Titulo 3', id_item: 3},
        {titulo: 'Titulo 4', id_item: 4},
        {titulo: 'Titulo 5', id_item: 5},
        {titulo: 'Titulo 6', id_item: 6},
        {titulo: 'Titulo 7', id_item: 7},
        {titulo: 'Titulo 8', id_item: 8},
        {titulo: 'Titulo 9', id_item: 9},
        {titulo: 'Titulo 10', id_item: 10},
        {titulo: 'Titulo 11', id_item: 11},
        {titulo: 'Titulo 12', id_item: 12},
        {titulo: 'Titulo 13', id_item: 13},
        {titulo: 'Titulo 14', id_item: 14},
        {titulo: 'Titulo 15', id_item: 15},
        {titulo: 'Titulo 16', id_item: 16},
        {titulo: 'Titulo 17', id_item: 17},
        {titulo: 'Titulo 18', id_item: 18},
        {titulo: 'Titulo 19', id_item: 19},
        {titulo: 'Titulo 20', id_item: 20},
        {titulo: 'Titulo 21', id_item: 21},
        {titulo: 'Titulo 22', id_item: 22},
        {titulo: 'Titulo 23', id_item: 23},
        {titulo: 'Titulo 24', id_item: 24},
      ]);
    });
};
