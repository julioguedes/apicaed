exports.up = function(knex, Promise) {
    return Promise.all([
      knex.schema.createTable('proxima_disponivel', function(table) {
        table.increments('id');
        table.integer('id_proxima');
      }),
    ]);
  };
  
  exports.down = function(knex, Promise) {
    return Promise.all([knex.schema.dropTable('proxima_disponivel')]);
  };
  
  exports.configuration = { transaction: true };
  