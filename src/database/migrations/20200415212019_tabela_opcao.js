exports.up = function(knex, Promise) {
    return Promise.all([
      knex.schema.createTable('opcao', function(table) {
        table.increments('id');
        table.string('valor', 250).notNullable();
        table.string('descricao', 250).notNullable();
        table.integer('id_chave')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable('chave');
      }),
    ]);
  };
  
  exports.down = function(knex, Promise) {
    return Promise.all([knex.schema.dropTable('opcao')]);
  };
  
  exports.configuration = { transaction: true };
  