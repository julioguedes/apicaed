exports.up = function(knex, Promise) {
    return Promise.all([
      knex.schema.createTable('item', function(table) {
        table.increments('id');
        table.string('item', 100).notNullable();
        table.string('referencia', 100).notNullable();
        table.string('sequencial', 100).notNullable();
        table.string('solicitacao', 100).notNullable();
        table
            .enu('situacao', ['DISPONIVEL', 'RESERVADA', 'CORRIGIDA', 'COM_DEFEITO'])
            .notNullable()
            .defaultTo('DISPONIVEL');
      }),
    ]);
  };
  
  exports.down = function(knex, Promise) {
    return Promise.all([knex.schema.dropTable('item')]);
  };
  
  exports.configuration = { transaction: true };
  