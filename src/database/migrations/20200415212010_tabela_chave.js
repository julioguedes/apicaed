exports.up = function(knex, Promise) {
    return Promise.all([
      knex.schema.createTable('chave', function(table) {
        table.increments('id');
        table.string('titulo', 250).notNullable();
        table.integer('id_item')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable('item');
      }),
    ]);
  };
  
  exports.down = function(knex, Promise) {
    return Promise.all([knex.schema.dropTable('chave')]);
  };
  
  exports.configuration = { transaction: true };
  