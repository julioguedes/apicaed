var bookshelf = require('../../db');
const Chave = require('../chave/chave.model');

var Item = bookshelf.Model.extend(
  {
    tableName: 'item',
    chave() {
      return this.hasMany(Chave, 'id');
    },
  },
  {
    withRelated: ['chave', 'chave.opcao'],
    async listar() {
      return this.fetchAll({ withRelated: this.withRelated });
    },
    async buscarReservadas() {
      const query = Item.query((item) => {
        item.where('situacao', '=', 'RESERVADA');
      });
      return query.fetchAll({ withRelated: this.withRelated });
    },
    async obter(id) {
      const relacionamentos = this.withRelated;
      return this.forge({ id: id }).fetch({ withRelated: relacionamentos });
    },
    async passarItemParaCorrigido(id) {
      return this.forge({ id: id, situacao: 'CORRIGIDA' }).save();
    },
    async passarItemParaReservado(id) {
      return this.forge({ id: id, situacao: 'RESERVADA' }).save();
    },
    async passarItemParaComDefeito(id) {
      return this.forge({ id: id, situacao: 'COM_DEFEITO' }).save();
    },
    async obterProximoDisponivel(id) {
      return await bookshelf
        .knex('item')
        .first('id')
        .where('situacao', '=', 'DISPONIVEL')
        .andWhere('id', '!=', id)
        .andWhere('id', '>', id);
    },
    async obterProximoReservada(id) {
      return await bookshelf
        .knex('item')
        .first('id')
        .where('situacao', '=', 'RESERVADA')
        .andWhere('id', '!=', id)
        .andWhere('id', '>', id);
    },
    async verificarAnteriorSeCorrigida(id) {
      return await bookshelf
        .knex('item')
        .first('id')
        .where('situacao', '=', 'DISPONIVEL')
        .andWhere('id', '<', id);
    },
  },
);

module.exports = Item;
