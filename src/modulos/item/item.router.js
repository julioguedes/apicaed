const express = require("express");
const controller = require("./item.controller");

const api = express.Router();
api.get("/correcoes/proxima", controller.proxima);
api.get("/correcoes/reservadas", controller.buscatodasreservadas);
api.post("/correcoes/:id", controller.correcao);
api.post("/correcoes/reservadas/:id", controller.reservada);
api.put("/correcoes/comdefeito/:id", controller.defeito);

module.exports = api;