const ItemService = require('./item.service');
const ItemModel = require('./item.model');
var _ = require('lodash');
const { NotFound } = require('../../exceptions');

module.exports.proxima = async (req, res, next) => {
  try {
    const { reservada } = req.query;
    const resposta = await ItemService.proxima(reservada);
    if (_.isEmpty(resposta)) {
      return res
        .status(404)
        .json({
          data: null,
          situacao: 'ERRO',
          tipo: 'SEM_CORRECAO',
          descricao: 'Não existem mais correções disponíveis',
        });
    }
    return res.status(200).json({ data: resposta });
  } catch (erro) {
    return next(erro);
  }
};

module.exports.buscatodasreservadas = async (req, res, next) => {
  try {
    const resposta = await ItemModel.buscarReservadas();
    if (resposta === null) {
      throw new NotFound('Nenhum item reservado foi encontrado');
    }
    return res.status(200).json(resposta);
  } catch (erro) {
    return next(erro);
  }
};

module.exports.defeito = async (req, res, next) => {
  try {
    await ItemModel.passarItemParaComDefeito(req.params.id);
    return res.status(200).json({data: 'Item com defeito salvo'});
  } catch (erro) {
    return next(erro);
  }
};

module.exports.correcao = async (req, res, next) => {
  try {
    const resposta = await ItemService.corrigir(req.params.id, req.body);
    return res.status(resposta.status).json(resposta.resposta);
  } catch (erro) {
    return next(erro);
  }
};

module.exports.reservada = async (req, res, next) => {
  try {
    const resposta = await ItemService.reservar(req.params.id, req.body);
    return res.status(resposta.status).json(resposta.resposta);
  } catch (erro) {
    return next(erro);
  }
};
