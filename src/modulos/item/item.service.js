const bookshelf = require('../../db');
const ProximaDisponivel = require('../proxima_disponivel/proxima_disponivel.model');
const ProximaReservada = require('../proxima_reservada/proxima_reservada.model');
const ItemModel = require('./item.model');
var _ = require('lodash');

const _regrasValidacao = async (id, dado) => {
  const resposta = await ItemModel.obter(id);
  const chave = resposta.toJSON().chave;
  const arrayValores = [];
  let objectErro = {};
  chave.forEach((dado) => {
    dado.opcao.forEach((opcao) => {
      arrayValores.push(opcao.valor);
    });
  });

  if (resposta.toJSON().situacao === 'CORRIGIDA') {
    objectErro = {
      situacao: 'ERRO',
      tipo: 'ITEM_CORRIGIDO',
      descricao: 'Item já corrigido',
    };
    return objectErro;
  }
  if (!_.includes(arrayValores, dado.chave[0].valor)) {
    objectErro = {
      situacao: 'ERRO',
      data: `Chave de correção incorreta. Valor ${dado.chave[0].valor} não é válido para o item ${id}`,
      tipo: 'CHAVE_INCORRETA',
    };
    return objectErro;
  }
  if (resposta.toJSON().situacao === 'RESERVADA') {
    return false;
  }
  const respostaSeAnteriorCorrigida = await ItemModel.verificarAnteriorSeCorrigida(id);
  if (respostaSeAnteriorCorrigida != undefined) {
    objectErro = {
      situacao: 'ERRO',
      tipo: 'ITEM_INVALIDO',
      descricao: 'Item inválido para correção',
    };
    return objectErro;
  }
  return false;
};

const _setarProximaDisponivel = async (id) => {
  let idProxima;
  await ProximaDisponivel.deletar(id);
  idProxima = await ItemModel.obterProximoDisponivel(id);
  if (idProxima != undefined) {
    await ProximaDisponivel.salvar(idProxima.id);
  }
};

const _setarProximaReservada = async (id) => {
  let idProxima;
  await ProximaReservada.deletar(id);
  idProxima = await ItemModel.obterProximoReservada(id);
  if (idProxima != undefined) {
    await ProximaReservada.salvar(idProxima.id);
  }
};

const proxima = async (reservada) => {
  try {
    let resposta;
    if (reservada === 'true') {
      resposta = await ProximaReservada.obter();
      await _setarProximaReservada(resposta.id_proxima);
    } else {
      resposta = await ProximaDisponivel.obter();
      await _setarProximaDisponivel(resposta.id_proxima);
    }
    return await ItemModel.obter(resposta.id_proxima);
  } catch (error) {
    return error;
  }
};

const corrigir = async (id, dado) =>
  // lembrar de voltar aqui e colocar tudo na transação
  bookshelf.transaction(async (transacao) => {
    try {
      let status;
      const resposta = await _regrasValidacao(id, dado);
      if (resposta !== false) {
        status = 404;
        return { resposta, status };
      }
      try {
        await ItemModel.passarItemParaCorrigido(id);
        status = 200;
        const resposta = {
          situacao: 'SUCESSO',
          descricao: 'Correção salva com sucesso',
        };
        await _setarProximaDisponivel(id);
        return { resposta, status };
      } catch (error) {
        return error;
      }
    } catch (error) {
      throw error;
    }
  });

const reservar = async (id, dado) =>
  bookshelf.transaction(async (transacao) => {
    try {
      let status;
      const resposta = await _regrasValidacao(id, dado);
      if (resposta !== false) {
        status = 404;
        return { resposta, status };
      }
      try {
        await ItemModel.passarItemParaReservado(id);
        status = 200;
        const resposta = {
          situacao: 'SUCESSO',
          descricao: 'Correção reservada com sucesso',
        };
        await _setarProximaReservada(id);
        return { resposta, status };
      } catch (error) {
        return error;
      }
    } catch (error) {
      throw error;
    }
  });

module.exports = {
  proxima,
  corrigir,
  reservar,
};
