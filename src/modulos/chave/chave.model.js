var bookshelf = require('../../db');
const Opcao = require('../opcao/opcao.model');

var Chave = bookshelf.Model.extend(
  {
    tableName: 'chave',
    opcao() {
      return this.hasMany(Opcao, 'id_chave');
    },
  },
);

module.exports = Chave;
