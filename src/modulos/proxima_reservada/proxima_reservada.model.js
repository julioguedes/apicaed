var bookshelf = require('../../db');

var ProximaReservada = bookshelf.Model.extend(
  {
    tableName: 'proxima_reservada',
  },
  {
    async obter() {
      return await bookshelf.knex('proxima_reservada').first('id_proxima');
    },
    async deletar(id) {
      await bookshelf.knex('proxima_reservada').where('id_proxima', '=', id).del();
    },
    async salvar(id) {
      return this
      .forge({id_proxima: id})
      .save();
    },
  },
);

module.exports = ProximaReservada;
