var bookshelf = require('../../db');

var ProximaDisponivel = bookshelf.Model.extend(
  {
    tableName: 'proxima_disponivel',
  },
  {
    async obter() {
      return await bookshelf.knex('proxima_disponivel').first('id_proxima');
    },
    async deletar(id) {
      await bookshelf.knex('proxima_disponivel').where('id_proxima', '=', id).del();
    },
    async salvar(id) {
      return this
      .forge({id_proxima: id})
      .save();
    },
  },
);

module.exports = ProximaDisponivel;
