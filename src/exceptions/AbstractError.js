class AbstractError extends Error {
  constructor(statusCode, message, data, situacao, tipo, descricao) {
    if (new.target === AbstractError) {
      throw new TypeError('A classe abstrata "AbstractError" não pode ser instanciada diretamente.');
    }
    super(message);
    this.name = this.constructor.name;
    this.message = message;
    this.statusCode = statusCode;
    this.errorCode = statusCode;
    this.data = data;
    this.situacao = situacao;
    this.tipo = tipo;
    this.descricao = descricao;
    Error.captureStackTrace(this, this.contructor);
  }
}

module.exports = AbstractError;
