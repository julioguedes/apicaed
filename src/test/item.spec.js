process.env.NODE_ENV = 'test';
process.env.PORT = 5000;
process.env.DB_HOST = 'localhost';
process.env.DB_NAME = 'caed';
process.env.DB_USER = 'root';
process.env.DB_PASS = '123456';

var chai = require('chai');
var server = require('../server');
const knex = require('../db').knex;
var chaiHttp = require('chai-http');
const { expect } = chai;
chai.should();

chai.use(chaiHttp);

describe('Testando funções para item', function () {
  let idItem1;
  let idItem2;
  let idItem3;
  let idItem4;
  let idItem5;
  let idItem6;
  let chave1;
  let chave2;
  let chave3;
  let chave4;
  let chave5;
  let chave6;
  // Faz cadastro dos item a serem testados
  before(async () => {
    idItem1 = await knex('item').insert({
      item: 'Item de Teste 1',
      referencia: 'upload/correcao_9859662.png',
      sequencial: '69300003130128',
      solicitacao: '2000000885',
      situacao: 'DISPONIVEL',
    });
    idItem2 = await knex('item').insert({
      item: 'Item de Teste 2',
      referencia: 'upload/correcao_9859662.png',
      sequencial: '69300003130128',
      solicitacao: '2000000885',
      situacao: 'DISPONIVEL',
    });
    idItem3 = await knex('item').insert({
      item: 'Item de Teste 3',
      referencia: 'upload/correcao_9859662.png',
      sequencial: '69300003130128',
      solicitacao: '2000000885',
      situacao: 'DISPONIVEL',
    });
    idItem4 = await knex('item').insert({
      item: 'Item de Teste 4',
      referencia: 'upload/correcao_9859662.png',
      sequencial: '69300003130128',
      solicitacao: '2000000885',
      situacao: 'CORRIGIDA',
    });
    idItem5 = await knex('item').insert({
      item: 'Item de Teste 5',
      referencia: 'upload/correcao_9859662.png',
      sequencial: '69300003130128',
      solicitacao: '2000000885',
      situacao: 'RESERVADA',
    });
    idItem6 = await knex('item').insert({
      item: 'Item de Teste 6',
      referencia: 'upload/correcao_9859662.png',
      sequencial: '69300003130128',
      solicitacao: '2000000885',
      situacao: 'DISPONIVEL',
    });
    chave1 = await knex('chave').insert({
      titulo: 'Chave 1 de Teste',
      id_item: idItem1[0],
    });
    chave2 = await knex('chave').insert({
      titulo: 'Chave 2 de Teste',
      id_item: idItem2[0],
    });
    chave3 = await knex('chave').insert({
      titulo: 'Chave 3 de Teste',
      id_item: idItem3[0],
    });
    chave4 = await knex('chave').insert({
      titulo: 'Chave 4 de Teste',
      id_item: idItem4[0],
    });
    chave5 = await knex('chave').insert({
      titulo: 'Chave 5 de Teste',
      id_item: idItem5[0],
    });
    chave6 = await knex('chave').insert({
      titulo: 'Chave 6 de Teste',
      id_item: idItem6[0],
    });
    await knex('opcao').insert({
      valor: '0',
      descricao: 'Sem descrição 1',
      id_chave: chave1[0],
    });
    await knex('opcao').insert({
      valor: '1',
      descricao: 'Sem descrição 2',
      id_chave: chave1[0],
    });

    await knex('opcao').insert({
      valor: '0',
      descricao: 'Sem descrição 3',
      id_chave: chave2[0],
    });
    await knex('opcao').insert({
      valor: '1',
      descricao: 'Sem descrição 4',
      id_chave: chave2[0],
    });

    await knex('opcao').insert({
      valor: '0',
      descricao: 'Sem descrição 5',
      id_chave: chave3[0],
    });
    await knex('opcao').insert({
      valor: '1',
      descricao: 'Sem descrição 6',
      id_chave: chave3[0],
    });

    await knex('opcao').insert({
      valor: '0',
      descricao: 'Sem descrição 7',
      id_chave: chave4[0],
    });
    await knex('opcao').insert({
      valor: '1',
      descricao: 'Sem descrição 8',
      id_chave: chave4[0],
    });


    await knex('opcao').insert({
      valor: '0',
      descricao: 'Sem descrição 9',
      id_chave: chave5[0],
    });
    await knex('opcao').insert({
      valor: '1',
      descricao: 'Sem descrição 10',
      id_chave: chave5[0],
    });


    await knex('opcao').insert({
      valor: '0',
      descricao: 'Sem descrição 12',
      id_chave: chave6[0],
    });
    await knex('opcao').insert({
      valor: '1',
      descricao: 'Sem descrição 12',
      id_chave: chave6[0],
    });
  });

  // Deleta o registro de todos os itens testados
  after(async () => {
    await knex('opcao').where('id_chave', '=', chave1[0]).del();
    await knex('opcao').where('id_chave', '=', chave2[0]).del();
    await knex('opcao').where('id_chave', '=', chave3[0]).del();
    await knex('opcao').where('id_chave', '=', chave4[0]).del();
    await knex('opcao').where('id_chave', '=', chave5[0]).del();
    await knex('opcao').where('id_chave', '=', chave6[0]).del();
    await knex('chave').where('id', '=', chave1[0]).del();
    await knex('chave').where('id', '=', chave2[0]).del();
    await knex('chave').where('id', '=', chave3[0]).del();
    await knex('chave').where('id', '=', chave4[0]).del();
    await knex('chave').where('id', '=', chave5[0]).del();
    await knex('chave').where('id', '=', chave6[0]).del();
    await knex('item').where('id', '=', idItem1[0]).del();
    await knex('item').where('id', '=', idItem2[0]).del();
    await knex('item').where('id', '=', idItem3[0]).del();
    await knex('item').where('id', '=', idItem4[0]).del();
    await knex('item').where('id', '=', idItem5[0]).del();
    await knex('item').where('id', '=', idItem6[0]).del();
  });

  //Correção salva com sucesso
  it('Salva uma correção com Sucesso', function (done) {
    chai
      .request(server)
      .post(`/correcoes/${idItem1}`)
      .send({
        chave: [
          {
            valor: '1',
          },
        ],
      })
      .end(function (err, res) {
        expect(res.body.descricao).to.equal('Correção salva com sucesso');
        done();
      });
  });

  //CHAVE_INCORRETA
  it('Não salva uma correção pois chave esta incorreta', function (done) {
    chai
      .request(server)
      .post(`/correcoes/${idItem2}`)
      .send({
        chave: [
          {
            valor: 'dsfAFSFasfASF',
          },
        ],
      })
      .end(function (err, res) {
        expect(res.body.tipo).to.equal('CHAVE_INCORRETA');
        done();
      });
  });

  //Item já corrigido
  it('Não salva pois o item já foi corrigido', function (done) {
    chai
      .request(server)
      .post(`/correcoes/${idItem4}`)
      .send({
        chave: [
          {
            valor: '0',
          },
        ],
      })
      .end(function (err, res) {
        expect(res.body.descricao).to.equal('Item já corrigido');
        done();
      });
  });

  //Item inválido para correção
  it('Não salva pois o item anterior ainda não foi corrigo', function (done) {
    chai
      .request(server)
      .post(`/correcoes/${idItem3}`)
      .send({
        chave: [
          {
            valor: '0',
          },
        ],
      })
      .end(function (err, res) {
        expect(res.body.descricao).to.equal('Item inválido para correção');
        done();
      });
  });

  //Item com defeito
  it('Item com Defeito', function (done) {
    chai
      .request(server)
      .put(`/correcoes/comdefeito/${idItem3}`)
      .end(function (err, res) {
        expect(res.body.data).to.equal('Item com defeito salvo');
        done();
      });
  });

  //Correçao reservada com sucesso
  it('Salva uma correção reservada com sucesso', function (done) {
    chai
      .request(server)
      .post(`/correcoes/reservadas/${idItem2}`)
      .send({
        chave: [
          {
            valor: '1',
          },
        ],
      })
      .end(function (err, res) {
        expect(res.body.descricao).to.equal('Correção reservada com sucesso');
        done();
      });
  });

  //CHAVE_INCORRETA
  it('Não salva um item reservado pois a chave é invalida', function (done) {
    chai
      .request(server)
      .post(`/correcoes/reservadas/${idItem6}`)
      .send({
        chave: [
          {
            valor: 'dadgasdgsadgasdga',
          },
        ],
      })
      .end(function (err, res) {
        expect(res.body.tipo).to.equal('CHAVE_INCORRETA');
        done();
      });
  });


    //Item já corrigifo
    it('Não salva um item reservado pois a chave é invalida', function (done) {
      chai
        .request(server)
        .post(`/correcoes/reservadas/${idItem4}`)
        .send({
          chave: [
            {
              valor: '0',
            },
          ],
        })
        .end(function (err, res) {
          expect(res.body.descricao).to.equal('Item já corrigido');
          done();
        });
    });


  //Correção reservada salva com sucesso, mesmo que a anterior nao tenha sido corrigada
  it('Salva uma correção reservada com sucesso mesmo com a anterior ainda não corrida', function (done) {
    chai
      .request(server)
      .post(`/correcoes/reservadas/${idItem5}`)
      .send({
        chave: [
          {
            valor: '1',
          },
        ],
      })
      .end(function (err, res) {
        expect(res.body.descricao).to.equal('Correção reservada com sucesso');
        done();
      });
  });


  //Lista todas as reservadas
  it('Lista todos os itens reservados', function (done) {
    chai
      .request(server)
      .get('/correcoes/reservadas')
      .end(function (err, res) {
        res.should.have.status(200);
        done();
      });
  });
});
