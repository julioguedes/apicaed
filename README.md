#Instruções de Uso
    1. Banco Usado é o MYSQL
    2. Criar um banco com o nome de caed (as infos estão no .env)
        {
            DB_HOST=localhost
            DB_NAME=caed
            DB_USER=root
            DB_PASS=123456 
        }
    3. Instalar as dependencias -> "npm install"
    4. Rodar o migrate:  "npm run migrate:up"
    5. Para rodar os testes basta entrar no diretorio src e executar o comando "mocha"
    6. Para popular o bando de dados foi criado um seed com item, para rodar "npm run seed:run"
    7. Para dropar a base "npm run migrate:down"
    8. Para rodar a aplicação npm run serve

#Requisições
    api.get("/correcoes/proxima")  -> bsuca proxima disponivel
    api.get("/correcoes/reservadas") -> busca proxima reservada
    api.post("/correcoes/:id") -> grava a correção passando o id do item
    api.post("/correcoes/reservadas/:id") -> reserva correção passando o id do item
    api.put("/correcoes/comdefeito/:id") -> seta para COM_DEFEITO passando o item do item